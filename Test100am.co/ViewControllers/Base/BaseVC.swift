//
//  BaseVC.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 16.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func showAlertError(_ error : NSError) {
    
    self.showAlertMessage(error.localizedDescription)
  }

  func showAlertMessage(_ message : String) {
    
    let alert = UIAlertController.init(title: "Error", message: message, preferredStyle: .alert)
    let okAction = UIAlertAction.init(title: "OK", style: .cancel, handler: { (action) in
      alert.dismiss(animated: true, completion: nil)
    })
    
    alert.addAction(okAction)
    self.present(alert, animated: true, completion: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

}
