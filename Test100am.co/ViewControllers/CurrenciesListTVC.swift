//
//  CurrenciesListTVC.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit
import CoreData

enum SelectedTypeCurrency {
  
  case none
  case source
  case target
}

protocol CurrenciesListTVCDelegate {
  func selectCurrency(_ currency: Currency, inTVC: CurrenciesListTVC)
}

class CurrenciesListTVC: UITableViewController {

  var fetchResultsController : NSFetchedResultsController<NSFetchRequestResult>!
  var delegate : CurrenciesListTVCDelegate?
  var typeCurrency : SelectedTypeCurrency = .none
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      self.setupFetchedResultsController()
  }
  
  override func viewWillAppear(_ animated: Bool) {

    super.viewWillAppear(animated)
    
    WebService.sharedInstance.loadCurrenciesFromServer()
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if self.fetchResultsController == nil {
      return 0
    }
    return (self.fetchResultsController.fetchedObjects?.count)!
  }
  
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "currencieCellID", for: indexPath)
    
    let currency = self.fetchResultsController.object(at: indexPath) as! Currency
    cell.textLabel?.text = currency.name
    cell.detailTextLabel?.text = currency.key?.uppercased()
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    tableView.deselectRow(at: indexPath, animated: true)
    
    let currency = fetchResultsController.object(at: indexPath) as! Currency
    self.delegate?.selectCurrency(currency, inTVC: self)
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  func setupFetchedResultsController() {
    
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Currency")
    
    let sortDescriptor = NSSortDescriptor(key: "key", ascending: true)
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: NSManagedObjectContext.mr_default(), sectionNameKeyPath: nil, cacheName: nil)
    
    fetchResultsController = frc
    fetchResultsController.delegate = self
    do {
      try fetchResultsController.performFetch()
    } catch {
      print(error)
    }
  }
}

extension CurrenciesListTVC : NSFetchedResultsControllerDelegate {
  
  func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.beginUpdates()
  }
  
  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    
    switch type {
      
    case .insert:
      if let indexPath = newIndexPath {
        tableView.insertRows(at: [indexPath], with: .automatic)
      }
      
    case .update:
      if let indexPath = indexPath {
        tableView.reloadRows(at: [indexPath], with: .automatic)
      }
      
    case .move:
      if let indexPath = indexPath {
        tableView.deleteRows(at: [indexPath], with: .automatic)
      }
      if let newIndexPath = newIndexPath {
        tableView.insertRows(at: [newIndexPath], with: .automatic)
      }
      
    case .delete:
      if let indexPath = indexPath {
        tableView.deleteRows(at: [indexPath], with: .automatic)
      }
    }
  }
  
  func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.endUpdates()
  }
}
