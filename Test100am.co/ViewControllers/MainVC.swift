//
//  MainVC.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class MainVC: BaseVC {
  
  @IBOutlet weak var tableView: UITableView!
  var fetchResultsController : NSFetchedResultsController<NSFetchRequestResult>!
  var refreshControl : UIRefreshControl?
  
  var editTableViewButtonItem : UIBarButtonItem?
  var addRateButtonItem : UIBarButtonItem?
  var doneEditButtonItem : UIBarButtonItem?
  
  override func viewDidLoad() {

    super.viewDidLoad()
    
    self.setupFetchedResultsController()

    self.refreshControl = UIRefreshControl.init()
    refreshControl?.addTarget(self, action: #selector(MainVC.reloadRates), for: .valueChanged)
    self.tableView.addSubview(refreshControl!)
    self.tableView.allowsSelectionDuringEditing = false
    
    self.addRateButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(self.createNewRate))
    self.editTableViewButtonItem = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(self.beginEditTableView))
    self.doneEditButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.doneEditTableView))

  }
  
  func createNewRate() {
    
    self.performSegue(withIdentifier: "createNewRateSegueID", sender: self)
  }
  
  @IBAction func beginEditTableView() {
    
    if self.tableView.isEditing == false {
      self.tableView.setEditing(true, animated: true)
      
      self.navigationItem.rightBarButtonItem = self.doneEditButtonItem
      self.navigationItem.leftBarButtonItem = nil
    }
  }
  
  @IBAction func doneEditTableView() {
    
    if self.tableView.isEditing == true {
      self.tableView.setEditing(false, animated: true)
      
      self.navigationItem.rightBarButtonItem = self.addRateButtonItem
      self.navigationItem.leftBarButtonItem = self.editTableViewButtonItem
      
    }

  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    self.navigationItem.rightBarButtonItem = self.addRateButtonItem
    self.navigationItem.leftBarButtonItem = self.editTableViewButtonItem

  }
  
  func reloadRates() {
    
    self.refreshControl?.beginRefreshing()
    WebService.sharedInstance.loadRatesFromServer { (error) in
      
      if error != nil {
        self.showAlertError(error!)
      }
      self.refreshControl?.endRefreshing()
    }
  }
  
  func setupFetchedResultsController() {
    
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Rate")
    
    let sortDescriptor = NSSortDescriptor(key: "sortIndex", ascending: true)
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: NSManagedObjectContext.mr_default(), sectionNameKeyPath: nil, cacheName: nil)
    
    fetchResultsController = frc
    fetchResultsController.delegate = self
    do {
      try fetchResultsController.performFetch()
    } catch {
      print(error)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    if segue.identifier == "openRateSegueID" {
      
      if let indexPath = tableView.indexPathForSelectedRow {
        let rate = fetchResultsController.object(at: indexPath) as! Rate
        (segue.destination as! DetailCurrencyVC).rate = rate
      }
    }
  }
  
}

extension MainVC : UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {

    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

    if self.fetchResultsController == nil {
      return 0
    }
    return (self.fetchResultsController.fetchedObjects?.count)!
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "rateCellId", for: indexPath)
    
    let rate = self.fetchResultsController.object(at: indexPath) as! Rate
    cell.textLabel?.text = "\((rate.sourceCurrency?.key)!) -> \((rate.targetCurrency?.key)!)"
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

    let context = NSManagedObjectContext.mr_context(withParent: self.fetchResultsController.managedObjectContext)
    
    var rates = self.fetchResultsController.fetchedObjects as! [Rate]

    let rate = rates[sourceIndexPath.row]
    
    rates.remove(at: sourceIndexPath.row)
    rates.insert(rate, at: destinationIndexPath.row)

    for rate in rates {
      rate.sortIndex = rates.index(of: rate)! + 1
    }
    
    context.mr_saveToPersistentStoreAndWait()
  }

  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    
    if editingStyle == .delete {

      let context = NSManagedObjectContext.mr_context(withParent: self.fetchResultsController.managedObjectContext)
      
      var rates = self.fetchResultsController.fetchedObjects as! [Rate]
      
      let rate = rates[indexPath.row]
      
      rates.remove(at: indexPath.row)
      rate.mr_deleteEntity()
      for rate in rates {
        rate.sortIndex = rates.index(of: rate)! + 1
      }
      
      context.mr_saveToPersistentStoreAndWait()
    }
  }

}

extension MainVC : NSFetchedResultsControllerDelegate {
  
  func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.beginUpdates()
  }
  
  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    
    switch type {
      
    case .insert:
      if let indexPath = newIndexPath {
        tableView.insertRows(at: [indexPath], with: .automatic)
      }
      
    case .update:
      if let indexPath = indexPath {
        tableView.reloadRows(at: [indexPath], with: .automatic)
      }
      
    case .move:
      if let indexPath = indexPath {
        tableView.deleteRows(at: [indexPath], with: .automatic)
      }
      if let newIndexPath = newIndexPath {
        tableView.insertRows(at: [newIndexPath], with: .automatic)
      }
      
    case .delete:
      if let indexPath = indexPath {
        tableView.deleteRows(at: [indexPath], with: .automatic)
      }
    }
  }
  
  func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.endUpdates()
  }

}
