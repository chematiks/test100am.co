//
//  DetailCurrencyVC.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit
import CoreData

class DetailCurrencyVC: BaseVC {
  
  @IBOutlet weak var sourceCurrencyLabel: UILabel!
  @IBOutlet weak var targetCurrencyLabel: UILabel!
  @IBOutlet weak var valueLabel: UILabel!
  @IBOutlet weak var deltaLabel: UILabel!
  @IBOutlet weak var timeUpdateLabel: UILabel!

  var rate : Rate? {
    
    didSet {
      self.showRate()
    }
  }
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    
    NotificationCenter.default.removeObserver(self)
    
    super.viewDidDisappear(animated)
  }

  override func didReceiveMemoryWarning() {
    
    super.didReceiveMemoryWarning()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
    super.viewWillAppear(animated)
    self.deltaLabel.clipsToBounds = true
    self.deltaLabel.layer.cornerRadius = 3.0
    self.showRate()
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(self.managedObjectContextObjectsDidChange),
                                           name: NSNotification.Name.NSManagedObjectContextObjectsDidChange,
                                           object: self.rate?.managedObjectContext)
  }
  
  func managedObjectContextObjectsDidChange(notification: NSNotification) {

    self.rate = rate?.mr_(in: (self.rate?.managedObjectContext)!)
  }
  
  func showRate() {
    
    if rate == nil {
      self.sourceCurrencyLabel.text = "-"
      self.targetCurrencyLabel.text = "-"
      self.deltaLabel.text = "0.0"
      self.deltaLabel.backgroundColor = .darkGray
      self.valueLabel.text = "-"
      return
    }else{
      self.sourceCurrencyLabel?.text = "\((rate?.sourceCurrency?.name)!)\n\((rate?.sourceCurrency?.key)!)"
      self.targetCurrencyLabel?.text = "\((rate?.targetCurrency?.name)!)\n\((rate?.targetCurrency?.key)!)"
      self.title = "\((rate?.sourceCurrency?.key)!)->\((rate?.targetCurrency?.key)!)"
      if ((rate?.rateValue) != nil) {
        self.valueLabel?.text = String(format: "%.4f",(rate?.rateValue)!)
      }
      let delta : Double = (self.rate?.lastDelta)!
      var sign = ""
      if delta > 0 {
        sign = "+"
      }
      self.deltaLabel?.text = sign + String(format: "%.4f",delta)

      if delta > 0.0 {
        self.deltaLabel?.backgroundColor = .green
      }else if delta < 0.0{
        self.deltaLabel?.backgroundColor = .red
      }else{
        self.deltaLabel?.backgroundColor = .darkGray
      }
      
      let isToday = rate?.lastUpdate?.isToday()
      if isToday != nil {
        let dateFormatter = DateFormatter.init()
        dateFormatter.timeStyle = isToday! ? .short : .none
        dateFormatter.dateStyle = isToday! ? .none : .short
        
        self.timeUpdateLabel?.text = dateFormatter.string(from: (rate?.lastUpdate)! as Date)
      }else{
        self.timeUpdateLabel?.text = ""
      }
      
    }
    
  }
}

