//
//  AddCurrencyVC.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddCurrencyVC: BaseVC {
  
  @IBOutlet weak var sourceButton: UIButton!
  @IBOutlet weak var targetButton: UIButton!
  
  @IBOutlet weak var cancelButtonItem: UIBarButtonItem!
  @IBOutlet weak var saveButtonItem: UIBarButtonItem!
  
  
  var sourceCurrency : Currency? {
    didSet {
      if sourceCurrency != nil {
        self.sourceButton.setTitle("\((sourceCurrency?.name)!)\n\((sourceCurrency?.key)!)", for: .normal)
      }else{
        self.sourceButton.setTitle("None", for: .normal)
      }
      self.updateInterface()
    }
  }
  
  var targetCurrency : Currency? {
    didSet {
      if targetCurrency != nil {
        self.targetButton.setTitle("\((targetCurrency?.name)!)\n\((targetCurrency?.key)!)", for: .normal)
      }else{
        self.targetButton.setTitle("None", for: .normal)
      }
      self.updateInterface()

    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.updateInterface()

  }
  
  func updateInterface() {
    
    self.saveButtonItem.isEnabled = sourceCurrency != nil && targetCurrency != nil
  }

  @IBAction func cancelButtonPress() {
    
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction func saveButtonPress() {
   
    if self.sourceCurrency == self.targetCurrency {
      self.showIdenticalCurrencyAlert()
    }else{
      let context = NSManagedObjectContext.mr_default()
      let rate = Rate.mr_createEntity(in: context)
      if rate != nil {
        rate?.sourceCurrency = sourceCurrency
        rate?.targetCurrency = targetCurrency
        rate?.sortIndex = Int16(Rate.mr_numberOfEntities())
        context.mr_saveToPersistentStore(completion: { (success, error) in
          WebService.sharedInstance.loadNewRateFromServer(rate!, completion: { (error) in
            
          })
        })
        
      }
      
      _ = self.navigationController?.popViewController(animated: true)
    }
  }
  
  func showIdenticalCurrencyAlert() {

    self.showAlertMessage("It is necessary to choose different currencies")    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    if let destinationVC = segue.destination as? CurrenciesListTVC {

      destinationVC.delegate = self
      if segue.identifier == "selectSourceCurrencySegueID" {
        destinationVC.typeCurrency = .source
      }else{
        destinationVC.typeCurrency = .target
      }
    }
  }

  @IBAction func sourceButtonPress(_ sender: Any) {
    performSegue(withIdentifier: "selectSourceCurrencySegueID", sender: self)
  }
  
  @IBAction func targetButtonPress(_ sender: Any) {
    performSegue(withIdentifier: "selectTargetCurrencySegueID", sender: self)
  }
  
}

extension AddCurrencyVC : CurrenciesListTVCDelegate {
  
  func selectCurrency(_ currency: Currency, inTVC: CurrenciesListTVC) {
    
    if inTVC.typeCurrency == .source {
      self.sourceCurrency = currency
    }else if inTVC.typeCurrency == .target {
      self.targetCurrency = currency
    }
  }
}
