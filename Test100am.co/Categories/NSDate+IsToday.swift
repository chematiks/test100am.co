//
//  NSDate+IsToday.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 15.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation

extension NSDate {
  
  func isToday() -> Bool {

    return NSCalendar.current.isDateInToday(self as Date)
  }
}
