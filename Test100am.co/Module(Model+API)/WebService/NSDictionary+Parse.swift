//
//  NSDictionary+Parse.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 16.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation
import CoreData

extension NSDictionary {
  
  func parseAndSaveToStore() {
    
    let context = NSManagedObjectContext.mr_context(withParent: NSManagedObjectContext.mr_default())
    
    if let currencies : NSDictionary = self.object(forKey: "currencies") as! NSDictionary? {
      self.parsingCurriencies(currencies, context: context)
    }else if let rates : NSDictionary = self.object(forKey: "quotes") as! NSDictionary? {
      let sourceKey = self.object(forKey: "source") as! String
      self.parsingRates(rates, sourceKey: sourceKey, context: context)
    }
    
  }
  
  private func parsingCurriencies(_ currencies : NSDictionary, context : NSManagedObjectContext) {
    let keys = currencies.allKeys
    
    for key in keys {
      _ = Currency.initFromDictionary([key : currencies[key] as! String], context: context)
      
    }
    
    context.mr_saveToPersistentStoreAndWait()
  }
  
  private func parsingRates(_ ratesDict : NSDictionary, sourceKey : String, context : NSManagedObjectContext) {
    
    let keys = ratesDict.allKeys as! [String]
    
    let ratesDictCropped : NSMutableDictionary = [:]
    
    for key in keys {
      
      if key.contains(sourceKey) {
        if key.hasPrefix(sourceKey)
        {
          let from = key.index(key.startIndex, offsetBy: sourceKey.lengthOfBytes(using: .utf8))
          let cropingKey = key.substring(from: from)
          ratesDictCropped.setValue(ratesDict.object(forKey: key) as! Double, forKey: cropingKey)
        }
      }
    }
    
    let rates = Rate.mr_findAll(in: context) as! [Rate]
    
    for rate in rates {
      
      if let sourceValue : Double = ratesDictCropped.object(forKey: rate.sourceCurrency?.key!) as? Double {
        
        if let targetValue : Double = ratesDictCropped.object(forKey: rate.targetCurrency?.key!) as? Double {
          
          var newValue = sourceValue / targetValue
          let delta = newValue - rate.rateValue
          if delta == newValue {
            rate.lastDelta = 0.0
            rate.rateValue = newValue
          }else if delta != 0.0 {
            rate.lastDelta = delta
            rate.rateValue = newValue
          }
          rate.lastUpdate = NSDate()
        }
      }
    }
    
    context.mr_saveToPersistentStoreAndWait()
  }
  
}
