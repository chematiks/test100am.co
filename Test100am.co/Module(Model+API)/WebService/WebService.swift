//
//  WebService.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

let serverURL = "http://apilayer.net/api/"
let apiAccessKey = "94feb1fab9eafee6135e6d6b3c78da5e"

class WebService : NSObject {
  
  static let sharedInstance = WebService()
  
  func accessKey() -> String {
    return "access_key=\(apiAccessKey)"
  }
  
  //MARK: Currency  

  func loadCurrenciesFromServer() {

    let url = URL.init(string: "\(serverURL)list?\(accessKey())")
    
    Alamofire.request(url!, method: .get, parameters: nil , encoding: JSONEncoding.default).responseJSON { (response) in

      switch response.result {
      case .success(let data):
        
        if let webResponse = data as? NSDictionary {
          webResponse.parseAndSaveToStore()
        }
        
        print("\(data as! NSDictionary)")
      case .failure(let error):
        print("\(error.localizedDescription)")
      }
    }
  }
  
  //MARK: Rate
  
  func allCurrenciesParameter() -> String {
    
    let predicate = NSPredicate.init(format: "sources.@count > 0 OR targets.@count > 0")
    let currencies = Currency.mr_findAll(with: predicate) as! [Currency]
  
    if currencies.count == 0 {
      return ""
    }
    
    var parameter = "&currencies="
    
    for currency in currencies {
      parameter = parameter + currency.key! + ","
    }
    
    return parameter
  }
  
  func loadRatesFromServer(_ completion: @escaping (_ error : NSError?) -> Void) {
    
    let parameter = self.allCurrenciesParameter()
    
    if parameter.characters.count == 0 {
      return
    }
    let url = URL.init(string: "\(serverURL)live?\(accessKey())\(parameter)")
    
    Alamofire.request(url!, method: .get, parameters: nil , encoding: JSONEncoding.default).responseJSON { (response) in
      
      switch response.result {
      case .success(let data):
        
        if let webResponse = data as? NSDictionary {
          webResponse.parseAndSaveToStore()
        }

        LoadingManager.sharedManager.lastUpdateTimestamp = NSDate().timeIntervalSince1970

        completion(nil)
        print("\(data as! NSDictionary)")
      case .failure(let error):
        print("\(error.localizedDescription)")
        completion(error as NSError?)
      }
    }
  }
  
  func loadNewRateFromServer(_ rate: Rate, completion: @escaping (_ error : NSError?) -> Void) {
    
    let parameter = "&currencies=\((rate.sourceCurrency?.key)!),\((rate.targetCurrency?.key)!)"
    
    let url = URL.init(string: "\(serverURL)live?\(accessKey())\(parameter)")
    
    Alamofire.request(url!, method: .get, parameters: nil , encoding: JSONEncoding.default).responseJSON { (response) in
      
      switch response.result {
      case .success(let data):
        
        if let webResponse = data as? NSDictionary {
          webResponse.parseAndSaveToStore()
        }
        
        completion(nil)
        print("\(data as! NSDictionary)")
      case .failure(let error):
        print("\(error.localizedDescription)")
        completion(error as NSError?)
      }
    }
    
  }
  
}
