//
//  LoadingManager.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 16.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit

let refreshRateTimeInSec = 5.0

class LoadingManager: NSObject {

  static let sharedManager = LoadingManager()
  
  var timer : Timer?
  
  var lastUpdateTimestamp : TimeInterval = 0.0
  
  func turnOnUpdatingTimer() {
    
    if timer?.isValid == true {
      timer?.invalidate()
    }
    
    if self.lastUpdateTimestamp + refreshRateTimeInSec >= NSDate().timeIntervalSince1970 {
      
      WebService.sharedInstance.loadRatesFromServer({ (error) in

      })
    }
    
    
    timer = Timer.scheduledTimer(withTimeInterval: refreshRateTimeInSec, repeats: true, block: { (timer) in
      
      print("refresh rates")
      
      WebService.sharedInstance.loadRatesFromServer({ (error) in

      })
      
    })
  }
  
  func turnOffUpdatingTimer() {
    
    if timer?.isValid == true {
      timer?.invalidate()
    }
    
  }
  
  
}
