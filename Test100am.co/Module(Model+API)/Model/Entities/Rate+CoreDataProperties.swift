//
//  Rate+CoreDataProperties.swift
//  
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//
//

import Foundation
import CoreData

extension Rate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Rate> {
        return NSFetchRequest<Rate>(entityName: "Rate");
    }
  
    @NSManaged public var rateValue: Double
    @NSManaged public var lastDelta: Double
    @NSManaged public var lastUpdate: NSDate?
    @NSManaged public var sortIndex: Int16
    @NSManaged public var sourceCurrency: Currency?
    @NSManaged public var targetCurrency: Currency?

}
