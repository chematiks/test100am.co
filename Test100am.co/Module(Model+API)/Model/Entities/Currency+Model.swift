//
//  Currency+Model.swift
//  Test100am.co
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation
import CoreData

extension Currency {
  
  class func initFromDictionary(_ dict : NSDictionary, context: NSManagedObjectContext) -> Currency? {
    
    let key = dict.allKeys.first as! String
    var currency = Currency.mr_findFirst(byAttribute: "key", withValue: key)
    if currency == nil {
      currency =  Currency.mr_createEntity(in: context)
      if currency != nil {
        if dict.allKeys.count > 0 {
          currency?.key = key
          currency?.name = dict.value(forKey: key) as! String?
        }
      }
    }
    return currency
  }
}
