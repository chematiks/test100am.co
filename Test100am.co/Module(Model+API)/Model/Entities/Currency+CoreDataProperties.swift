//
//  Currency+CoreDataProperties.swift
//  
//
//  Created by ALEXANDER CHANCHIKOV on 14.02.17.
//
//

import Foundation
import CoreData


extension Currency {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Currency> {
        return NSFetchRequest<Currency>(entityName: "Currency");
    }

    @NSManaged public var index: Int64
    @NSManaged public var key: String?
    @NSManaged public var name: String?
    @NSManaged public var sources: NSSet?
    @NSManaged public var targets: NSSet?

}

// MARK: Generated accessors for sources
extension Currency {

    @objc(addSourcesObject:)
    @NSManaged public func addToSources(_ value: Rate)

    @objc(removeSourcesObject:)
    @NSManaged public func removeFromSources(_ value: Rate)

    @objc(addSources:)
    @NSManaged public func addToSources(_ values: NSSet)

    @objc(removeSources:)
    @NSManaged public func removeFromSources(_ values: NSSet)

}

// MARK: Generated accessors for targets
extension Currency {

    @objc(addTargetsObject:)
    @NSManaged public func addToTargets(_ value: Rate)

    @objc(removeTargetsObject:)
    @NSManaged public func removeFromTargets(_ value: Rate)

    @objc(addTargets:)
    @NSManaged public func addToTargets(_ values: NSSet)

    @objc(removeTargets:)
    @NSManaged public func removeFromTargets(_ values: NSSet)

}
